//Requires Libraries:
//http://playground.arduino.cc/Code/PIDLibrary
//http://playground.arduino.cc/Code/NewPing

//PID
#include <PID_v1.h>


//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Define the aggressive and conservative Tuning Parameters
double aggKp=4, aggKi=0.2, aggKd=1;
double consKp=4, consKi=1, consKd=0.5;

//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, consKp, consKi, consKd, DIRECT);


#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX



//PING
#include <NewPing.h>
#define TRIGGER_PIN  8  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     7  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 500 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  mySerial.begin(4800);
   //initialize the variables we're linked to
  Input = 0;
  Setpoint = 40;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void loop() {
  delay(29);                      // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS).
  Serial.print("Ping: ");
  Serial.print(uS / US_ROUNDTRIP_CM); // Convert ping time to distance in cm and print result (0 = outside set distance range)
  Serial.println("cm");
  mySerial.println(uS / US_ROUNDTRIP_CM);
  Input = uS / US_ROUNDTRIP_CM;
  
  double gap = Setpoint-Input; //distance away from setpoint
  if(gap>0)//<10)
  {  //we're close to setpoint, use conservative tuning parameters
    //SetControllerDirection(DIRECT); 
  }
  else
  {
     //we're far from setpoint, use aggressive tuning parameters
     //SetControllerDirection(REVERSE);
  }
  
  myPID.Compute();
  Serial.println(Output);
}
